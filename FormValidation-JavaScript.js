/* Lab 3: Form validation page JavaScript */
function formValidation()
{
    var date = document.getElementById('event-date');
    var time = document.getElementById('event-time');
    var location = document.getElementById('event-location');
    var title = document.getElementById('event-title');
    var phone = document.getElementById('event-phone');
    var email = document.getElementById('event-email');
    var coordinator = document.getElementById('event-coordinator');
    var description = document.getElementById('event-description');

    var phoneFormat = /^\d{3}-\d{3}-\d{4}$/;
    var emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (date.value == "" || time.value == "" || location.value == "" || title.value == "" || phone.value == "" || email.value == "" || coordinator.value == "" || description.value == "")
    {
        window.alert("Please make sure every field is filled in.");
    } else if (location.value.length < 10 || location.value.length > 100)
    {
        window.alert("Please make sure that the location field is between 10 and 100 characters long.");
        location.focus();
    } else if (title.value.length < 1 || title.value.length > 50)
    {
        window.alert("Please make sure that the title field is between 1 and 50 characters long.");
        title.focus();
    } else if (!phone.value.match(phoneFormat))
    {
        window.alert("Please make sure that the phone number matches the xxx-xxx-xxxx format.")
        phone.focus();
    } else if (!email.value.match(emailFormat))
    {
        window.alert("Please make sure that the email address follows the placeholder@example.com format.")
        email.focus();
    } else if (description.value.length < 10 || description.value.length > 200)
    {
        window.alert("Please make sure that the description field is between 10 and 200 characters long.");
        description.focus();
    } else
    {
        document.write('<p id="jsText">The following data was submitted:<br>Date: ' + date.value + '<br>Time: ' + time.value + '<br>Location: ' + location.value + '<br>Title: ' + title.value + '<br>Description: ' + description.value + '</p>');
    }
}